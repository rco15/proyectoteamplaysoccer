package com.example.germandelafuente.teamplaysoccer;

import android.content.Context;
import android.support.v4.view.ViewGroupCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.*;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.germandelafuente.teamplaysoccer.clases.Persona;

import java.util.ArrayList;

/**
 * Created by german.delafuente on 06-06-2016.
 */
public class PersonaAdapter extends BaseAdapter {
    private final Context context;
    private final Persona[] values;

    public PersonaAdapter(Context context, Persona[] values){
        this.context = context;
        this.values = values;
    }

    @Override
    public int getCount() {
        return values.length;
    }

    @Override
    public Object getItem(int position) {
        return values[position];
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder;

        if (convertView == null) {
            // Create a new view into the list.
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.activity_listar_jugador, parent, false);

            viewHolder = new ViewHolder();

            viewHolder.rut = (TextView) convertView.findViewById(R.id.rut);
            viewHolder.nombre = (TextView) convertView.findViewById(R.id.nombre);
            viewHolder.apellido = (TextView) convertView.findViewById(R.id.apellido);
            viewHolder.equipo = (TextView) convertView.findViewById(R.id.equipo);
            viewHolder.numeroCami = (TextView) convertView.findViewById(R.id.numeroCami);
            viewHolder.promedio = (TextView) convertView.findViewById(R.id.promedio);

            convertView.setTag(viewHolder);

        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        Persona persona = (Persona)getItem(position);

        viewHolder.nombre.setText(persona.getNombre());
        viewHolder.apellido.setText(persona.getApellido());
        viewHolder.rut.setText(persona.getRut());
        viewHolder.equipo.setText(persona.getEquipo());
        viewHolder.numeroCami.setText(""+persona.getNumeroCami());
        viewHolder.promedio.setText(""+persona.getPromedio());

        return convertView;

    }


    public static class ViewHolder {

        public TextView rut;
        public TextView nombre;
        public TextView apellido;
        public TextView equipo;
        public TextView numeroCami;
        public TextView promedio;

    }
}
