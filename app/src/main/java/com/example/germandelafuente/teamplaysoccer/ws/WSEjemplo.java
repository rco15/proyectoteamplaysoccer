package com.example.germandelafuente.teamplaysoccer.ws;

import android.os.StrictMode;

import com.example.germandelafuente.teamplaysoccer.clases.Persona;
import com.example.germandelafuente.teamplaysoccer.clases.Usuario;
import com.google.gson.Gson;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import java.io.IOException;
import java.util.concurrent.TimeUnit;




/**
 * Created by salemlabs on 04-05-16.
 */
public class WSEjemplo {

    private static final long TIME_OUT_SECONDS = 6000;
    private static WSEjemplo instance;

    public static WSEjemplo getInstance() {
        if (instance == null) {
            instance = new WSEjemplo();
        }
        return instance;
    }

    public WSEjemplo(){}

    public Persona[] listadoPersonas(){
        Persona[] values = null;

        String url = "https://script.google.com/macros/s/AKfycbzZmWexGsvyO5VPD42LseT23X0Pea_LcihUiKwYcW7nA_rv5og/exec";
        try {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();

            StrictMode.setThreadPolicy(policy);
            String respuesta = getRequest(url);
            Gson gson = new Gson();
            values = gson.fromJson(respuesta, Persona[].class);

        } catch (Exception e) {

        }

        return values;
    }

    public Usuario[] listadoUsuarios(){
        Usuario[] values = null;

        String url = "https://script.google.com/macros/s/AKfycbzq5ol1O07twr3gxsKc0ayAf-MowoxNfh2vyYYwuWEnvBJHT7lq/exec";
        try {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();

            StrictMode.setThreadPolicy(policy);
            String respuesta = getRequest(url);
            Gson gson = new Gson();
            values = gson.fromJson(respuesta, Usuario[].class);

        } catch (Exception e) {

        }

        return values;
    }


    public String registroUsuario(Usuario values )
    {
        String respuesta = "";
        String url = "https://script.google.com/macros/s/AKfycbzq5ol1O07twr3gxsKc0ayAf-MowoxNfh2vyYYwuWEnvBJHT7lq/exec";
        FormEncodingBuilder formBody = new FormEncodingBuilder()
                .add("id", values.getId())
                .add("nombreUsu", values.getNombreUsu())
                .add("passUsu", values.getPassUsu())
                .add("nomEquipo", values.getNomEquipo())
                .add("region",values.getRegion())
                .add("comuna",values.getComuna())
                .add("activo",values.getActivo())  ;



        try {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();

            StrictMode.setThreadPolicy(policy);
            respuesta = postRequest(url, formBody);


        } catch (Exception e) {
            e.printStackTrace();
        }
        return respuesta;
    }


    public String registroPersonas(Persona values )
    {
        String respuesta = "";
        String url = "https://script.google.com/macros/s/AKfycbzZmWexGsvyO5VPD42LseT23X0Pea_LcihUiKwYcW7nA_rv5og/exec";
        FormEncodingBuilder formBody = new FormEncodingBuilder()
                .add("rut", values.getRut())
                .add("nombre", values.getNombre())
                .add("apellido", values.getApellido())
                .add("equipo", values.getEquipo())
                .add("numeroCami",""+values.getNumeroCami())
                .add("promedio",values.getPromedio());



        try {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();

            StrictMode.setThreadPolicy(policy);
            respuesta = postRequest(url, formBody);


        } catch (Exception e) {
            e.printStackTrace();
        }
        return respuesta;
    }


/*
    public RespuestaPost registroPersona(Context context, Persona values) {
        RespuestaPost retorno = null;
        FormEncodingBuilder formBody = new FormEncodingBuilder()
                .add("id", values.getId())
                .add("nombre", values.getNombre())
                .add("apellido", values.getApellido())
                .add("rut", values.getRut());

        String url = context.getResources().getString(R.string.url_servicio);
        try {
            String respuesta = postRequest(url, formBody);

            final GsonBuilder gsonBuilder = new GsonBuilder();
            gsonBuilder.registerTypeAdapter(RespuestaPost.class, new AgregarPersonaGSON());
            final Gson gson = gsonBuilder.create();
            retorno = gson.fromJson(respuesta, RespuestaPost.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return retorno;
    }

    public RespuestaGet listarPersonas(Context context) {
        RespuestaGet retorno = null;

        String url = context.getResources().getString(R.string.url_servicio);
        try {
            String respuesta = getRequest(url);

            final GsonBuilder gsonBuilder = new GsonBuilder();
            gsonBuilder.registerTypeAdapter(RespuestaGet.class, new ListarPersonaGSON());
            final Gson gson = gsonBuilder.create();
            retorno = gson.fromJson(respuesta, RespuestaGet.class);
        } catch (Exception e) {

        }
        return retorno;
    }*/

    private String postRequest(String url, FormEncodingBuilder params) throws IOException {
        OkHttpClient client = new OkHttpClient();
        client.setConnectTimeout(TIME_OUT_SECONDS, TimeUnit.SECONDS); // connect timeout
        client.setReadTimeout(TIME_OUT_SECONDS, TimeUnit.SECONDS);

        RequestBody formBody = params.build();

        Request request = new Request.Builder()
                .url(url)
                .post(formBody)
                .build();

        Response response = client.newCall(request).execute();
        if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);
        String respuesta = response.body().string();
        return respuesta;
    }

    private String getRequest(String url)  throws IOException {
        OkHttpClient client = new OkHttpClient();
        client.setConnectTimeout(TIME_OUT_SECONDS, TimeUnit.SECONDS); // connect timeout
        client.setReadTimeout(TIME_OUT_SECONDS, TimeUnit.SECONDS);
        Request request = new Request.Builder()
                .url(url)
                .get()
                .addHeader("cache-control", "no-cache")
                .build();

        Response response = client.newCall(request).execute();
        if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);
        String respuesta = response.body().string();
        return respuesta;
    }


}
