package com.example.germandelafuente.teamplaysoccer;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.Spinner;
import android.widget.Toast;


import com.example.germandelafuente.teamplaysoccer.bd.DataBaseManager;
import com.example.germandelafuente.teamplaysoccer.clases.Usuario;
import com.example.germandelafuente.teamplaysoccer.ws.WSEjemplo;
import com.google.gson.Gson;

import java.util.List;
import java.util.Random;

public class RegistrarseActivity extends AppCompatActivity {


    private Button btnGuardar;
    private EditText txtNombreUsu;
    private EditText txtPassUsu;
    private EditText txtNomEquipo;
    private   Spinner spinnerR;
    private Spinner spinnerC;

    /*DataBaseManager dataBaseManager;
    Cursor cursor;
    private ListView listView;
    SimpleCursorAdapter simpleCursorAdapter;*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registrarse);
         /*dataBaseManager = new DataBaseManager(this);
        cursor = dataBaseManager.cargarCursosrUsuarios();
        listView = (ListView) findViewById(R.id.listView);
        String[] from = new String[]{dataBaseManager.CM_ID,dataBaseManager.CM_JSON_USUARIOS};
        int[] to = new int[]{android.R.id.text1,android.R.id.text2};
        simpleCursorAdapter = new SimpleCursorAdapter(this,android.R.layout.two_line_list_item,cursor,from,to,0);

        listView.setAdapter(simpleCursorAdapter);*/


         spinnerR= (Spinner) findViewById(R.id.region_spinner);
         spinnerC = (Spinner) findViewById(R.id.comuna_spinner);
// Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.region_array,  R.layout.spinner_item);
        ArrayAdapter<CharSequence> adapterC = ArrayAdapter.createFromResource(this, R.array.comuna_array_Metro,  R.layout.spinner_item);
// Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        adapterC.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
// Apply the adapter to the spinner
        spinnerR.setAdapter(adapter);


        spinnerC.setAdapter(adapterC);



        txtNombreUsu = (EditText)findViewById(R.id.txtNombreUsu);
        txtPassUsu = (EditText)findViewById(R.id.txtPassUsu);
        txtNomEquipo = (EditText)findViewById(R.id.txtNomEquipo);


        btnGuardar = (Button)findViewById(R.id.btnGuardar);
        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                guadarUsuario();
            }
        });

        cargarDatosSesion();



    }



    private void guadarUsuario() {

        String  region =  spinnerR.getSelectedItem().toString();
        String  comuna =  spinnerC.getSelectedItem().toString();
        Random randomGenerator = new Random();
        int id = randomGenerator.nextInt(10000);
        Usuario usu = new Usuario();

                usu.setId(""+id);
                usu.setNombreUsu(    txtNombreUsu.getText().toString());
                usu.setPassUsu(   txtPassUsu.getText().toString());
                usu.setNomEquipo(txtNomEquipo.getText().toString());
                usu.setRegion(region);
                usu.setComuna( comuna);
                usu.setActivo(""+1);


        WSEjemplo sd = new WSEjemplo();
        String respuesta = sd.registroUsuario(usu);
        if(respuesta.equals("{\"status\": \"OK\"}"))
        {
            Toast.makeText(this, "Usuario Guardado", Toast.LENGTH_LONG).show();

            limpiar();

            Intent i = new Intent(this, loginActivity.class);
            startActivity(i);


        }
        else
        {
            Toast.makeText(this, "Error al guardar usuario : "+ respuesta, Toast.LENGTH_LONG).show();

            limpiar();

        }
  /*      Gson gson = new Gson();
        String usuarioJson = gson.toJson(usu);
        SesionPreferences.getInstance().guardarUsuario(this, usuarioJson);
*/


    }

    public void limpiar()
    {

        txtNombreUsu.setText("");
         txtPassUsu.setText("");
         txtNomEquipo.setText("");
         spinnerR.setSelection(0);
         spinnerC.setSelection(0);
    }


    private void cargarDatosSesion(){

    }


}
