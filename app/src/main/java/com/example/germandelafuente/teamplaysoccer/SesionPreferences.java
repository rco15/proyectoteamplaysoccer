package com.example.germandelafuente.teamplaysoccer;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.example.germandelafuente.teamplaysoccer.clases.Persona;
import com.example.germandelafuente.teamplaysoccer.clases.Usuario;
import com.google.gson.Gson;

import java.util.List;
import java.util.Map;

/**
 * Created by Duoc on 18-05-2016.
 */
public class SesionPreferences {

    private static final String NOMBRE_SHARED = "sesion_usuario";
    private static final String KEY_NOMBRE = "key_nombre";
  /*  private static final String KEY_NOMBRE_USUARIO = "key_nombre_usuario";
    private static final String KEY_PASS_USUARIO = "key_pass_usuario";
    private static final String KEY_NOM_EQUIPO_CLIENTE = "key_nom_equipo_cliente";
    private static final String KEY_REGUION_CLIENTE = "key_nom_equipo_cliente";
    private static final String KEY_COMUNA_CLIENTE = "key_nom_equipo_cliente";
*/
    public static final String KEY_USUARIO_JSON = "key_usuario_json";

    public static final String KEY_PERSONA_JSON = "key_persona_json";
    private static SesionPreferences instance;
    
    private SesionPreferences(){}
    
    public static SesionPreferences getInstance(){
        
        if(instance == null){
            instance = new SesionPreferences();            
        }
        return instance;
    }

    public void guardarSesion(Context context, String nombre) {
        SharedPreferences sha = context.getSharedPreferences(NOMBRE_SHARED, Context.MODE_PRIVATE);
        SharedPreferences.Editor edit = sha.edit();
        edit.putString(KEY_NOMBRE, nombre);
        edit.commit();
    }

    public void cerrarSesion(Context context) {
        SharedPreferences sha = context.getSharedPreferences(NOMBRE_SHARED, Context.MODE_PRIVATE);
        SharedPreferences.Editor edit = sha.edit();
        edit.putString(KEY_NOMBRE, "");
        edit.commit();
    }

    public String obtenerSesion(Context context) {
        SharedPreferences sha = context.getSharedPreferences(NOMBRE_SHARED, Context.MODE_PRIVATE);
        String nombre = sha.getString(KEY_NOMBRE, "");
        return nombre;
    }

    public void guardarPersona(Context context, String usuarioJson) {
        SharedPreferences sha = context.getSharedPreferences(NOMBRE_SHARED, Context.MODE_PRIVATE);
        SharedPreferences.Editor edit = sha.edit();
        edit.putString(KEY_PERSONA_JSON, usuarioJson);
        edit.commit();
    }


    public void guardarUsuario(Context context, String usuarioJson) {
        SharedPreferences sha = context.getSharedPreferences(NOMBRE_SHARED, Context.MODE_PRIVATE);
        SharedPreferences.Editor edit = sha.edit();
        edit.putString(KEY_USUARIO_JSON, usuarioJson);
        edit.commit();
    }

   public Usuario obtenerUsuario(Context context) {
        Usuario  usuario = null;
        SharedPreferences sha = context.getSharedPreferences(NOMBRE_SHARED, Context.MODE_PRIVATE);
        try {
            String usuarioJson = sha.getString(KEY_USUARIO_JSON, "");
            Gson gson = new Gson();
            usuario = gson.fromJson(usuarioJson, Usuario.class);
        }catch (Exception e){
        }

        return usuario;
    }

    public String obtenerPersonaString(Context context) {
        String usuarioJson = null;
        SharedPreferences sha = context.getSharedPreferences(NOMBRE_SHARED, Context.MODE_PRIVATE);
        try {
             usuarioJson = sha.getString(KEY_PERSONA_JSON, "");
            /*Gson gson = new Gson();
            per = gson.fromJson(usuarioJson, Persona.class);*/
        }catch (Exception e){
        }

        return usuarioJson;
    }

    public Persona obtenerPersona(Context context) {
        Persona per = null;
        SharedPreferences sha = context.getSharedPreferences(NOMBRE_SHARED, Context.MODE_PRIVATE);
        try {
            String usuarioJson = sha.getString(KEY_PERSONA_JSON, "");
            Gson gson = new Gson();
            per = gson.fromJson(usuarioJson, Persona.class);
        }catch (Exception e){
        }

        return per;
    }

    public List<Usuario> obtenerTodosUsuario(Context context) {
        List<Usuario>  usuarios = null;
        Usuario  usuario = null;

        int x = 1;
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);

        SharedPreferences sha = context.getSharedPreferences(NOMBRE_SHARED, Context.MODE_PRIVATE);

        Map<String, ?> all = sha.getAll();
        try {
            Map<String, ?>   usuariosJson = preferences.getAll();

            for (Map.Entry<String, ?> entry : usuariosJson.entrySet())
            {
                usuario =(Usuario) entry.getValue();
                usuarios.add(usuario);
                x++;
            }


        }catch (Exception e){
        }

        return usuarios;
    }


    
}












