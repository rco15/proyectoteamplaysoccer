package com.example.germandelafuente.teamplaysoccer;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;


import com.example.germandelafuente.teamplaysoccer.clases.Usuario;

/**
 * Created by german.delafuente on 29-06-2016.
 */
public class EquipoAdapter extends BaseAdapter {
    private final Context context;
    private final Usuario[] values;

    public EquipoAdapter(Context context, Usuario[] values){
        this.context = context;
        this.values = values;
    }

    @Override
    public int getCount() {
        return values.length;
    }

    @Override
    public Object getItem(int position) {
        return values[position];
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder;

        if (convertView == null) {
            // Create a new view into the list.
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.activity_listar_equipo, parent, false);

            viewHolder = new ViewHolder();

            viewHolder.nombreUsu = (TextView) convertView.findViewById(R.id.nombreUsu);
            viewHolder.nombreEquipo = (TextView) convertView.findViewById(R.id.nombreEquipo);
            viewHolder.region = (TextView) convertView.findViewById(R.id.region);
            viewHolder.comuna = (TextView) convertView.findViewById(R.id.comuna);


            convertView.setTag(viewHolder);

        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        Usuario usu = (Usuario)getItem(position);

        viewHolder.nombreUsu.setText(usu.getNombreUsu());
        viewHolder.nombreEquipo.setText(usu.getNomEquipo());
        viewHolder.region.setText(usu.getRegion());
        viewHolder.comuna.setText(usu.getComuna());


        return convertView;

    }


    public static class ViewHolder {

        public TextView nombreUsu;
        public TextView nombreEquipo;
        public TextView region;
        public TextView comuna;


    }
}