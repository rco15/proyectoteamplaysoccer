package com.example.germandelafuente.teamplaysoccer.clases;

/**
 * Created by german.delafuente on 01-06-2016.
 */
public class Persona {
    private String rut;
    private String nombre;
    private String apellido;
    private String equipo;
    private String numeroCami;
    private String promedio;

    public String getRut() {
        return rut;
    }

    public void setRut(String rut) {
        this.rut = rut;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getEquipo() {
        return equipo;
    }

    public void setEquipo(String equipo) {
        this.equipo = equipo;
    }

    public String getNumeroCami() {
        return numeroCami;
    }

    public void setNumeroCami(String numeroCami) {
        this.numeroCami = numeroCami;
    }

    public String getPromedio() {
        return promedio;
    }

    public void setPromedio(String promedio) {
        this.promedio = promedio;
    }
}
