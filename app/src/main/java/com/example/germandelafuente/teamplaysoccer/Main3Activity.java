package com.example.germandelafuente.teamplaysoccer;

import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import android.widget.MediaController;
import android.widget.VideoView;

import com.example.germandelafuente.teamplaysoccer.clases.Tiempo;

public class Main3Activity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        //Quitamos barra de notificaciones

        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        //Quitamos barra de titulo de la aplicacion

        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main3);


        VideoView videoView = (VideoView) findViewById(R.id.videoView);

        //Video Loop
        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {

            @Override
            public void onPrepared(MediaPlayer mp) {
                // TODO Auto-generated method stub
                mp.setLooping(true);




            }


        });

        Uri path = Uri.parse("android.resource://com.example.germandelafuente.teamplaysoccer/"

                + R.raw.giphy);




        videoView.setVideoURI(path);

        videoView.start();

        Tiempo tiempo = new Tiempo();



        tiempo.Contar();


        /*for(int x = 10 ; x<=50000000;x++)
        {
            if(tiempo.getSegundos() >= 30) {
                Intent i = new Intent(this, MenuActivity.class);
                startActivity(i);
                break;
            }
        }*/


    }




}
