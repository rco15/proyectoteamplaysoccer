package com.example.germandelafuente.teamplaysoccer.bd;

import android.content.Context;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by german.delafuente on 01-06-2016.
 */
public class basedatosHelper extends SQLiteOpenHelper
{

    private static final String DB_NAME = "android_db.sqlite";

    private static final int DB_VERSION = 1 ;

    public basedatosHelper(Context context)
    {
        super(context, DB_NAME,null,DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db)
    {
        db.execSQL(DataBaseManager.CREATE_TABLE_USERS);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
