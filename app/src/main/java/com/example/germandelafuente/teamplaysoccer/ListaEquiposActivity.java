package com.example.germandelafuente.teamplaysoccer;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import com.example.germandelafuente.teamplaysoccer.clases.Persona;
import com.example.germandelafuente.teamplaysoccer.clases.Usuario;
import com.example.germandelafuente.teamplaysoccer.ws.WSEjemplo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;

/**
 * Created by german.delafuente on 29-06-2016.
 */
public class ListaEquiposActivity extends AppCompatActivity {

    ListView listView;
    EquipoAdapter adapter;
    ImageButton btnVolver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.lista_equipos);

        consultarDatos();

        listView = (ListView)findViewById(R.id.list_view2);

        btnVolver = (ImageButton)findViewById(R.id.btnvuelve2);


        btnVolver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                redireccionar(0);
            }
        });

    }

    private void consultarDatos() {
        new ObtieneDatos(this).execute();
    }

    private class ObtieneDatos extends AsyncTask<Void, Void, Usuario[]> {

        private final Context context;
        private ProgressDialog mProgress;

        public ObtieneDatos(Context context){
            this.context = context;
        }


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgress = new ProgressDialog(context);
            mProgress.setIndeterminate(true);
            mProgress.setMessage("Obteniendo Información");
            mProgress.setCancelable(false);
            mProgress.show();

        }

        @Override
        protected Usuario[] doInBackground(Void... params) {

            Usuario[] values = null;
            try{

                values = WSEjemplo.getInstance().listadoUsuarios();

            }catch (Exception e){

            }


            return values;
        }

        @Override
        protected void onPostExecute(Usuario[] usuario)
        {
            super.onPostExecute(usuario);
            mProgress.hide();
            cargarInformacion(usuario);
        }
    }

    private void cargarInformacion(Usuario[] usu) {
        if(usu != null) {

                Toast.makeText(this, "Se cargaron " + usu.length + " personas", Toast.LENGTH_LONG).show();

                adapter = new EquipoAdapter(this, usu);
                listView.setAdapter(adapter);

        }
    }



    public void redireccionar(int position)
    {

        Intent i = null;

        if(position == 0) {
            i = new Intent(this, MenuActivity.class);
            startActivity(i);
        }
        if(position == 1) {
            i = new Intent(this, AgregarPersonaActivity.class);
            startActivity(i);
        }
        if(position == 2) {
            i = new Intent(this, ListarJugadoresActivity.class);
            startActivity(i);
        }
        if(position == 3) {
           /* i = new Intent(this, AgregarPersonaActivity.class);
            startActivity(i);*/

            Toast.makeText(this,"No se encontro la opcion ",Toast.LENGTH_LONG).show();
        }
        if(position == 4) {
           /* i = new Intent(this, AgregarPersonaActivity.class);
            startActivity(i);*/

            Toast.makeText(this,"No se encontro la opcion ",Toast.LENGTH_LONG).show();
        }


    }
}