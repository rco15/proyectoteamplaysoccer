package com.example.germandelafuente.teamplaysoccer.bd;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.germandelafuente.teamplaysoccer.clases.Persona;

/**
 * Created by german.delafuente on 01-06-2016.
 */
public class DataBaseManager {

    private basedatosHelper helper;
    private SQLiteDatabase db;

    public DataBaseManager(Context context)
    {
        helper = new basedatosHelper(context);
        db = helper.getWritableDatabase();
    }


    // nombre de tabla usuario
    public static final String TABLE_USERS = "usuarios";

    //campos de la tabla usuarios
    public static final String CM_ID = "id";
    public static final String CM_JSON_USUARIOS = "json_usuarios";
   // public static final String CM_FECHA_ING = "fecha_ingreso";


    //sentencia de creacion de tabla usuarios
    public static final String CREATE_TABLE_USERS = "create table "+TABLE_USERS+" ("
            +CM_ID+" integer primary key autoincrement,"
            +CM_JSON_USUARIOS+" text not null ); ";
           // +CM_FECHA_ING+"date); ,";

    private ContentValues generarContenValues( String jsonUsu)
    {
        ContentValues cont = new ContentValues();
      // cont.put(CM_ID,id);
        cont.put(CM_JSON_USUARIOS,jsonUsu);
        return cont;
    }

    public void insertarUsu( String jsonUsu)
    {
        db.insert(TABLE_USERS,null,generarContenValues(jsonUsu));

    }

    public Cursor cargarCursosrUsuarios()
    {
        String[] cols = new String[]{CM_ID,CM_JSON_USUARIOS};
       return  db.query(TABLE_USERS,cols,null,null,null,null,null);
    }






}
