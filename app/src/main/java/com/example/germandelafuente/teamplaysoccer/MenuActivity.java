package com.example.germandelafuente.teamplaysoccer;


import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.germandelafuente.teamplaysoccer.clases.Usuario;


public class MenuActivity extends AppCompatActivity {


    ImageButton btnagregaJ, btnListaJ, btnFace,btnVideo,btnBuscarEquipos;
    TextView txtBien;
    public static String urlFace = " https://www.facebook.com/Teamplaysoccer-1641571926055926/?view_public_for=1641571926055926";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        txtBien = (TextView) findViewById(R.id.textTextoMenu);

        txtBien.setText(textoBienvenida());


        btnagregaJ = (ImageButton) findViewById(R.id.btnAgregarJugadores);
        btnListaJ = (ImageButton) findViewById(R.id.btnListarJugadores);
        btnFace = (ImageButton) findViewById(R.id.btnFace);
        btnVideo = (ImageButton) findViewById(R.id.btnVideo);
        btnBuscarEquipos = (ImageButton) findViewById(R.id.btnBuscarEquipos);


        btnagregaJ.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                redireccionar(1);

            }
        });

        btnListaJ.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                redireccionar(2);

            }
        });

        btnFace.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                redireccionar(10);

            }
        });
        btnVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                redireccionar(3);

            }
        });

        btnBuscarEquipos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                redireccionar(4);

            }
        });


        /* bottomNavigationBar = (BottomNavigationBar) findViewById(R.id.bottom_navigation_bar);

        bottomNavigationBar
                .addItem(new BottomNavigationItem(R.drawable.ic_home_white_24dp, "Inicio").setActiveColor(R.color.green))
                .addItem(new BottomNavigationItem(R.drawable.ic_favorite_white_24dp, "Agregar Jugador").setActiveColor(R.color.green))
                .addItem(new BottomNavigationItem(R.drawable.ic_book_white_24dp, "Lista Equipos").setActiveColor(R.color.green))
                .addItem(new BottomNavigationItem(R.drawable.ic_location_on_white_24dp, "Buscar").setActiveColor(R.color.green))
                .addItem(new BottomNavigationItem(R.drawable.ic_videogame_asset_white_24dp, "Mis partidos").setActiveColor(R.color.green))
                .addItem(new BottomNavigationItem(R.drawable.ic_favorite_white_24dp, "Ver mis Jugador").setActiveColor(R.color.green))
                .addItem(new BottomNavigationItem(R.drawable.ic_launch_white_24dp, "Salir").setActiveColor(R.color.green))

                .initialise();


        bottomNavigationBar.setTabSelectedListener(new BottomNavigationBar.OnTabSelectedListener(){
            @Override
            public void onTabSelected(int position)
            {ir(position); }
            @Override
            public void onTabUnselected(int position) {
            }
            @Override
            public void onTabReselected(int position) {
            }
        });*/

    }


    public void redireccionar(int position) {

        Intent i = null;

        if (position == 0) {
            i = new Intent(this, MenuActivity.class);
            startActivity(i);
        }
        if (position == 1) {
            i = new Intent(this, AgregarPersonaActivity.class);
            startActivity(i);
        }
        if (position == 2) {
            i = new Intent(this, ListarJugadoresActivity.class);
            startActivity(i);
        }
        if (position == 3) {
            i = new Intent(this, Main3Activity.class);
            startActivity(i);

            //Toast.makeText(this, "No se encontro la opcion ", Toast.LENGTH_LONG).show();
        }
        if (position == 4) {
            i = new Intent(this, ListaEquiposActivity.class);
            startActivity(i);


        }

        if (position == 10) {
           /* i = new Intent(this, AgregarPersonaActivity.class);
            startActivity(i);*/


            startActivity(abrirFace(this));

        }


    }

    public static Intent abrirFace(Context context) {
        try {

            context.getPackageManager().getPackageInfo("com.facebook.katana", 0);
            return new Intent(Intent.ACTION_VIEW, Uri.parse("fb://page/TeamPlaySoccer"));
        } catch (Exception e) {

            return new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.facebook.com/TeamPlaySoccer"));
        }
    }

    public String textoBienvenida()
    {


        Usuario usuario = SesionPreferences.getInstance().obtenerUsuario(this);

        return  "Bienvenido a nuestra aplicacion estimado " +usuario.getNombreUsu()+", desde ya te damos la " +
                " bienvenida y esperamos que disfrutes la aplicacion. " ;

        }








}

