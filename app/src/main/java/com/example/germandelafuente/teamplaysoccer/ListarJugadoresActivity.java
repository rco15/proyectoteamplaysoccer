package com.example.germandelafuente.teamplaysoccer;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInstaller;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import com.example.germandelafuente.teamplaysoccer.clases.Persona;
import com.example.germandelafuente.teamplaysoccer.clases.Usuario;
import com.example.germandelafuente.teamplaysoccer.ws.WSEjemplo;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;


/**
 * Created by german.delafuente on 06-06-2016.
 */
public class ListarJugadoresActivity  extends AppCompatActivity {

    ListView listView;
    PersonaAdapter adapter;
    ImageButton btnVolver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.lista_jugador);

        consultarDatos();

        listView = (ListView)findViewById(R.id.list_view1);

        btnVolver = (ImageButton)findViewById(R.id.btnvuelve);


        btnVolver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                redireccionar(0);
            }
        });

    }

    private void consultarDatos() {
        new ObtieneDatos(this).execute();
    }

    private class ObtieneDatos extends AsyncTask<Void, Void, Persona[]> {

        private final Context context;
        private ProgressDialog mProgress;

        public ObtieneDatos(Context context){
            this.context = context;
        }


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgress = new ProgressDialog(context);
            mProgress.setIndeterminate(true);
            mProgress.setMessage("Obteniendo Información");
            mProgress.setCancelable(false);
            mProgress.show();

        }

        @Override
        protected Persona[] doInBackground(Void... params) {

            Persona[] values = null;
            try{

                values = WSEjemplo.getInstance().listadoPersonas();

            }catch (Exception e){

            }


            return values;
        }

        @Override
        protected void onPostExecute(Persona[] personas)
        {
            super.onPostExecute(personas);
            mProgress.hide();
            cargarInformacion(personas);
        }
    }

    private void cargarInformacion(Persona[] personas) {
        if(personas != null) {
            Persona[] personas2 = encuentraEquipo(personas);

            if (personas2 != null) {
                Toast.makeText(this, "Se cargaron " + personas2.length + " personas", Toast.LENGTH_LONG).show();

                adapter = new PersonaAdapter(this, personas2);
                listView.setAdapter(adapter);
            }
        }
    }


    public Persona[] encuentraEquipo(Persona[] personas)
    {
        int c = 0;
        Usuario usuario = SesionPreferences.getInstance().obtenerUsuario(this);
        for(int x=0;x<personas.length;x++)
        {
            if(personas[x].getEquipo().trim().toLowerCase().toString().equals(usuario.getNomEquipo().trim().toLowerCase().toString()) )
            {
                c++;
            }
        }

        Persona[] perResuesta = new Persona[c];
        ArrayList<Persona> con =  new ArrayList<Persona>(Arrays.asList(personas));

        Iterator it = con.iterator();

        int contador = 0;
        Persona personaNue = null;

        while(it.hasNext())
        {
            Persona r =(Persona) it.next();


            if(r.getEquipo().trim().toLowerCase().toString().equals(usuario.getNomEquipo().trim().toLowerCase().toString()) )
            {

                personaNue = new Persona();

                personaNue.setRut(r.getRut());
                personaNue.setNombre(r.getNombre());
                personaNue.setApellido(r.getApellido());
                personaNue.setEquipo(r.getEquipo());
                personaNue.setNumeroCami(r.getNumeroCami());
                personaNue.setPromedio(r.getPromedio());
                perResuesta[contador] = personaNue;


                contador++;

            }
        }

        return perResuesta;

    }


    public void redireccionar(int position)
    {

        Intent i = null;

        if(position == 0) {
            i = new Intent(this, MenuActivity.class);
            startActivity(i);
        }
        if(position == 1) {
            i = new Intent(this, AgregarPersonaActivity.class);
            startActivity(i);
        }
        if(position == 2) {
            i = new Intent(this, ListarJugadoresActivity.class);
            startActivity(i);
        }
        if(position == 3) {
           /* i = new Intent(this, AgregarPersonaActivity.class);
            startActivity(i);*/

            Toast.makeText(this,"No se encontro la opcion ",Toast.LENGTH_LONG).show();
        }
        if(position == 4) {
           /* i = new Intent(this, AgregarPersonaActivity.class);
            startActivity(i);*/

            Toast.makeText(this,"No se encontro la opcion ",Toast.LENGTH_LONG).show();
        }


    }
}
