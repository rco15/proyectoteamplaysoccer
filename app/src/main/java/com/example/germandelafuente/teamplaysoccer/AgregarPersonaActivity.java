package com.example.germandelafuente.teamplaysoccer;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;


import com.example.germandelafuente.teamplaysoccer.clases.Persona;
import com.example.germandelafuente.teamplaysoccer.clases.Usuario;
import com.example.germandelafuente.teamplaysoccer.ws.WSEjemplo;
import com.google.gson.Gson;


import java.util.Random;

public class AgregarPersonaActivity extends AppCompatActivity {



    private Button btnGuardar;
    ImageButton btnVolver;
    private EditText txtRutp;
    private EditText txtNombreP;
    private EditText txtApellidoP;
    private EditText txtNumeroCamiP;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agregar_persona);


        txtRutp = (EditText) findViewById(R.id.txtRutP);
        txtNombreP = (EditText) findViewById(R.id.txtNombreP);
        txtApellidoP = (EditText) findViewById(R.id.txtApellido);
        txtNumeroCamiP = (EditText) findViewById(R.id.txtNumeroCamiP);


        btnGuardar = (Button) findViewById(R.id.btnGuardarPer);
        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                guadarPersona();
            }
        });


        btnVolver = (ImageButton) findViewById(R.id.btnVolver);

        btnVolver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                redireccionar(0);
            }
        });





    }

    private void guadarPersona() {


        Random randomGenerator = new Random();
        int id = randomGenerator.nextInt(10000);
        Persona usu = new Persona();
        String rut = formatear(txtRutp.getText().toString());


        Usuario usuario = SesionPreferences.getInstance().obtenerUsuario(this);

        String per = SesionPreferences.getInstance().obtenerPersonaString(this);
        usu.setNombre(    txtNombreP.getText().toString());
        usu.setApellido(   txtApellidoP.getText().toString());
        usu.setRut(rut);
        usu.setNumeroCami(txtNumeroCamiP.getText().toString());
        usu.setEquipo(usuario.getNomEquipo() );
        usu.setPromedio("0");


        WSEjemplo sd = new WSEjemplo();
        String respuesta = sd.registroPersonas(usu);
        if(respuesta.equals("{\"status\": \"OK\"}"))
        {
            Toast.makeText(this, "Usuario Guardado", Toast.LENGTH_LONG).show();

            limpiar();

        }
        else
        {
            Toast.makeText(this, "Error al guardar usuario : "+ respuesta, Toast.LENGTH_LONG).show();

            limpiar();

        }


    }

    public void limpiar()
    {

        txtRutp.setText("");
        txtNombreP.setText("");
        txtApellidoP.setText("");
        txtNumeroCamiP.setText("");

    }


    public void redireccionar(int position)
    {

        Intent i = null;

        if(position == 0) {
            i = new Intent(this, MenuActivity.class);
            startActivity(i);
        }
        if(position == 1) {
            i = new Intent(this, AgregarPersonaActivity.class);
            startActivity(i);
        }
        if(position == 2) {
            i = new Intent(this, ListarJugadoresActivity.class);
            startActivity(i);
        }
        if(position == 3) {
           /* i = new Intent(this, AgregarPersonaActivity.class);
            startActivity(i);*/

            Toast.makeText(this,"No se encontro la opcion ",Toast.LENGTH_LONG).show();
        }
        if(position == 4) {
           /* i = new Intent(this, AgregarPersonaActivity.class);
            startActivity(i);*/

            Toast.makeText(this,"No se encontro la opcion ",Toast.LENGTH_LONG).show();
        }


    }

    public String formatear(String rut){
        int cont=0;
        String format;
        if(rut.length() == 0){
            return "";
        }else{
            rut = rut.replace(".", "");
            rut = rut.replace("-", "");
            format = "-"+rut.substring(rut.length()-1);
            for(int i = rut.length()-2;i>=0;i--){
                format = rut.substring(i, i+1)+format;
                cont++;
                if(cont == 3 && i != 0){
                    format = "."+format;
                    cont = 0;
                }
            }
            return format;
        }
    }

}
