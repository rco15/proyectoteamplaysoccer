package com.example.germandelafuente.teamplaysoccer;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.StrictMode;
import android.renderscript.Element;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.germandelafuente.teamplaysoccer.clases.Persona;
import com.example.germandelafuente.teamplaysoccer.clases.Usuario;
import com.example.germandelafuente.teamplaysoccer.ws.WSEjemplo;
import com.google.gson.Gson;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class loginActivity extends AppCompatActivity {

    EditText txtUsu,txtPass;
    Button btnEntra,btnRegistra;

    private static Usuario[] usuario;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activitylogin);
        if(isNetDisponible() && isOnlineNet()) {
            consultarDatos();

        }
        else
        {

            Toast.makeText(this, "Necesitas Acceso a internet para poder ingresar ", Toast.LENGTH_SHORT).show();
        }

        txtUsu = (EditText)findViewById(R.id.txtUsu);
        txtPass = (EditText)findViewById(R.id.txtPass);
        btnEntra = (Button)findViewById(R.id.btnEntra);
        btnRegistra = (Button)findViewById(R.id.btnRegistra);

        btnEntra.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                verifica(txtUsu.getText().toString(),txtPass.getText().toString());
            }
        });


        btnRegistra.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                irRegistro();

            }
        });

    }


    public void irRegistro()
    {
        Intent i = new Intent(this,RegistrarseActivity.class);

        startActivity(i);

    }


    private void consultarDatos() {
        new ObtieneDatos(this).execute();
    }
    private class ObtieneDatos extends AsyncTask<Void, Void, Usuario[]> {

        private final Context context;
        private ProgressDialog mProgress;

        public ObtieneDatos(Context context){
            this.context = context;
        }


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgress = new ProgressDialog(context);
            mProgress.setIndeterminate(true);
            mProgress.setMessage("Obteniendo Información");
            mProgress.setCancelable(false);
            mProgress.show();

        }

        @Override
        protected Usuario[] doInBackground(Void... params) {

            Usuario[] values = null;
            try{

                values = WSEjemplo.getInstance().listadoUsuarios();

            }catch (Exception e){

            }


            return values;
        }

        @Override
        protected void onPostExecute(Usuario[] usu)
        {
            super.onPostExecute(usu);
            mProgress.hide();
           // cargarInformacion(personas);
            usuario = usu;

        }
    }
    public void verifica(String usu,String pass)
    {



        if(usuario == null)
        {
            if(isNetDisponible() && isOnlineNet()) {
                WSEjemplo asda = new  WSEjemplo();
                usuario = asda.listadoUsuarios();

                verifica(usu,pass);
            }
            else
            {
                clear();
                Toast.makeText(this, "Necesitas Acceso a internet para poder ingresar ", Toast.LENGTH_SHORT).show();
            }
        }else {

            ArrayList<Usuario> con = new ArrayList<Usuario>(Arrays.asList(usuario));


            Iterator it = con.iterator();
            Boolean encontro = false;

            while (it.hasNext()) {
                Usuario r = (Usuario) it.next();
                if (r.getNombreUsu().equals(usu) && r.getPassUsu().equals(pass)) {
                    Intent i = new Intent(this, MenuActivity.class);
                    Gson gson = new Gson();

                    String usuarioJson = gson.toJson(r);
                    SesionPreferences.getInstance().guardarUsuario(this, usuarioJson);

                    startActivity(i);

                    encontro = true;
                    break;

                }
            }
            if (!encontro) {
                Toast.makeText(this, "Error Al Ingresar usuario y Password ", Toast.LENGTH_SHORT).show();
                clear();
            }

        }
    }

    public void clear()
    {
        txtUsu.setText("");
        txtPass.setText("");

    }


    private boolean isNetDisponible() {

        ConnectivityManager connectivityManager = (ConnectivityManager)
                getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo actNetInfo = connectivityManager.getActiveNetworkInfo();

        return (actNetInfo != null && actNetInfo.isConnected());
    }
    public Boolean isOnlineNet() {

        try {
            Process p = java.lang.Runtime.getRuntime().exec("ping -c 1 www.google.es");

            int val           = p.waitFor();
            boolean reachable = (val == 0);
            return reachable;

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return false;
    }

}
